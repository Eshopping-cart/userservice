package com.shopcart.user.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import common.shopcart.library.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDTO {
    @UsernameConstraint
    private String username;

    @NotEmpty
    @NameConstraint
    private String firstName;

    private String middleName;

    @NotEmpty
    @NameConstraint
    private String lastName;

    @Valid
    @NotNull
    private Address address;

    @PhoneNumberConstraint
    private String phoneNumber;

    @PasswordConstraint
    private String password;

    @EmailIdConstraint
    private String emailId;
}
