package com.shopcart.user.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import common.shopcart.library.constraints.EmailIdConstraint;
import common.shopcart.library.constraints.NameConstraint;
import common.shopcart.library.constraints.PhoneNumberConstraint;
import common.shopcart.library.constraints.UsernameConstraint;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserResponseDTO {

    @UsernameConstraint
    private String username;

    @EmailIdConstraint
    private String emailId;

    @NameConstraint
    private String firstName;

    private String middleName;

    @NotEmpty
    @NameConstraint
    private String lastName;

    @Valid
    @NotNull
    private Address address;

    @PhoneNumberConstraint
    private String phoneNumber;
}
