package com.shopcart.user.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.shopcart.user.models.enums.CountryName;
import common.shopcart.library.constraints.ZipcodeConstraint;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Address {

    private String flatNumber;

    @NotEmpty
    private String streetAddress1;

    private String streetAddress2;

    @NotEmpty
    private String landmark;

    @NotEmpty
    private String city;

    @NotEmpty
    @ZipcodeConstraint
    private String zipCode;

    @NotEmpty
    private String state;

    private CountryName country;
}
