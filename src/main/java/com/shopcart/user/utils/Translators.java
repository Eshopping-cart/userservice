package com.shopcart.user.utils;

import com.shopcart.user.models.UserDTO;
import com.shopcart.user.models.UserResponseDTO;
import com.shopcart.user.repository.model.UserDocument;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.stereotype.Component;

@Component
public class Translators {

    private final MapperFactory mapperFactory;

    private final MapperFacade mapperFacade;

    public Translators() {
        this.mapperFactory = new DefaultMapperFactory.Builder().build();
        mapperFactory.classMap(UserDTO.class, UserDocument.class).byDefault().register();
        mapperFactory.classMap(UserDocument.class , UserResponseDTO.class).exclude("password").byDefault().register();
        mapperFacade = mapperFactory.getMapperFacade();
    }

    public UserDocument toUserDocument(UserDTO userDTO) {
        UserDocument userDocument = mapperFacade.map(userDTO, UserDocument.class);
        return userDocument;
    }

    public UserResponseDTO toUserResponseDTO(UserDocument userDocument) {
        UserResponseDTO userResponseDTO = mapperFacade.map(userDocument, UserResponseDTO.class);
        return userResponseDTO;
    }
}
