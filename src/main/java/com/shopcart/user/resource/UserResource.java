package com.shopcart.user.resource;

import com.shopcart.user.models.UserResponseDTO;
import com.shopcart.user.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user/{username}")
public class UserResource {

	private final UserService userService;

	@Autowired
	public UserResource(UserService userService) {
		this.userService = userService;
	}

	@ApiOperation(value = "Get User details by Username" , response = UserResponseDTO.class , tags = "User Service",
			produces = MediaType.APPLICATION_JSON_VALUE , consumes = MediaType.APPLICATION_JSON_VALUE)
	@GetMapping
	public UserResponseDTO getUserDetails(@PathVariable("username") String username){
		return userService.getUserDetails(username);
	}
	
}
