package com.shopcart.user.resource;

import com.shopcart.user.models.UserDTO;
import com.shopcart.user.models.UserResponseDTO;
import com.shopcart.user.repository.model.UserDocument;
import com.shopcart.user.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/user/new/{username}")
public class NewUserResource {

	private final UserService userService;

	@Autowired
	public NewUserResource(UserService userService) {
		this.userService = userService;
	}

	@ApiOperation(value = "Create a new user", response = UserResponseDTO.class , tags = "User Service",
			produces = MediaType.APPLICATION_JSON_VALUE , consumes = MediaType.APPLICATION_JSON_VALUE)
	@PostMapping
	public UserResponseDTO createUser(@RequestBody @Valid UserDTO userDTO){
		return userService.createUser(userDTO);
	}
	
}
