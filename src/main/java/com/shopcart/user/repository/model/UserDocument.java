package com.shopcart.user.repository.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.shopcart.user.models.Address;
import common.shopcart.library.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "userInfo")
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDocument {
	@Id
	@UsernameConstraint
	private String username;

	@PasswordConstraint
	private String password;

	@Indexed
	@EmailIdConstraint
	private String emailId;

	@NameConstraint
	private String firstName;

	private String middleName;

	@NotEmpty
	@NameConstraint
	private String lastName;

	@Valid
	@NotNull
	private Address address;

	@PhoneNumberConstraint
	private String phoneNumber;
}
