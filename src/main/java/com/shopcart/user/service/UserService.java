package com.shopcart.user.service;

import com.shopcart.user.models.UserDTO;
import com.shopcart.user.models.UserResponseDTO;
import com.shopcart.user.repository.UserRepository;
import com.shopcart.user.repository.model.UserDocument;
import com.shopcart.user.utils.Translators;
import com.shopcart.user.exceptions.BadRequestException;
import com.shopcart.user.exceptions.RecordNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class UserService {

    private final UserRepository userRepository;

    private final Translators translators;

    @Autowired
    public UserService(UserRepository userRepository, Translators translators) {
        this.userRepository = userRepository;
        this.translators = translators;
    }

    public UserResponseDTO getUserDetails(String username) {
        UserDocument userDocument = userRepository.findById(username).orElseThrow(
                () -> new RecordNotFoundException("Cannot find user with username = "+username));
        return translators.toUserResponseDTO(userDocument);
    }

    public UserResponseDTO createUser(UserDTO userDTO){
        Optional<UserDocument> isExistingUser = userRepository.findById(userDTO.getUsername());
        if(isExistingUser.isPresent()) {
            log.info("User already present with the given username = {}",userDTO.getUsername());
            throw new BadRequestException("User with username = " +userDTO.getUsername() + " already present");
        }
        isExistingUser = userRepository.findByEmailId(userDTO.getEmailId());
        if(isExistingUser.isPresent()) {
            log.info("User already present with the given emailId = {}",userDTO.getEmailId());
            throw new BadRequestException("User with emailId = " +userDTO.getEmailId() + " already present");
        }
        UserDocument userDocument =  userRepository.save(translators.toUserDocument(userDTO));
        return translators.toUserResponseDTO(userDocument);
    }
}
