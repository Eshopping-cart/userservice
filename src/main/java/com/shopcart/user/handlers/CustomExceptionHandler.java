package com.shopcart.user.handlers;

import com.shopcart.user.exceptions.BadRequestException;
import com.shopcart.user.exceptions.RecordNotFoundException;
import com.shopcart.user.exceptions.UserNotAuthorizedException;
import common.shopcart.library.envelope.Envelope;
import common.shopcart.library.envelope.model.ErrorResponse;
import common.shopcart.library.envelope.model.StatusEnum;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(RecordNotFoundException.class)
    public final ResponseEntity handleResourceNotFoundException(RecordNotFoundException ex , WebRequest request) {
        final Envelope envelope = new Envelope();
        envelope.setStatus(StatusEnum.ERROR);
        envelope.setError(new ErrorResponse(HttpStatus.NOT_FOUND.value() , ex.getLocalizedMessage() , null));
        return new ResponseEntity(envelope , HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(BadRequestException.class)
    public final ResponseEntity handleBadRequestException(BadRequestException ex , HttpServletRequest request) {
        final Envelope envelope = new Envelope();
        envelope.setStatus(StatusEnum.ERROR);
        envelope.setError(new ErrorResponse(HttpStatus.BAD_REQUEST.value(), ex.getLocalizedMessage() , null));
        return new ResponseEntity(envelope , HttpStatus.BAD_REQUEST) ;
    }

    @ExceptionHandler(UserNotAuthorizedException.class)
    public final ResponseEntity handleUserNotAuthorizedException(UserNotAuthorizedException ex , HttpServletRequest request) {
        final Envelope envelope = new Envelope();
        envelope.setStatus(StatusEnum.ERROR);
        envelope.setError(new ErrorResponse(HttpStatus.UNAUTHORIZED.value() , ex.getLocalizedMessage() , null));
        return new ResponseEntity(envelope , HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(Throwable.class)
    public final ResponseEntity handleHttpServerErrorException(Throwable ex , HttpServletRequest request) {
        final Envelope envelope = new Envelope();
        envelope.setStatus(StatusEnum.ERROR);
        envelope.setError(new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getLocalizedMessage() , null));
        return new ResponseEntity(envelope , HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(HttpClientErrorException.class)
    public final ResponseEntity handleHttpClientErrorException(HttpClientErrorException ex , HttpServletRequest request) {
        final Envelope envelope = new Envelope();
        envelope.setStatus(StatusEnum.ERROR);
        envelope.setError(new ErrorResponse(ex.getStatusCode().value(), ex.getLocalizedMessage(), null));
        return new ResponseEntity(envelope , ex.getStatusCode());
    }
}
